<?php

/**
 * @file
 * WWU Email Autocomplete admin settings.
 */

/**
 * Form constructor for WWU Email Autocomplete settings form.
 */
function wwu_email_autocomplete_settings_form() {
  $form = array();
  $ldap_servers = ldap_servers_get_servers(NULL, 'all');

  if (!empty($ldap_servers)) {
    $server_options = array();

    foreach ($ldap_servers as $sid => $ldap_server) {
      $server_options[$sid] = $ldap_server->name;
    }

    reset($server_options);

    $form['wwu_email_autocomplete_sid'] = array(
      '#type' => 'radios',
      '#title' => t('LDAP Server'),
      '#description' => t('The LDAP server to query for email addresses.'),
      '#required' => TRUE,
      '#default_value' => variable_get('wwu_email_autocomplete_sid', key($server_options)),
      '#options' => $server_options,
    );
  }
  else {
    drupal_set_message('There are no LDAP servers configured. Configure an LDAP server before setting up Email Autocomplete.', 'error');
  }

  $form['wwu_email_autocomplete_basedn'] = array(
    '#type' => 'textarea',
    '#title' => t('Base DN'),
    '#required' => TRUE,
    '#rows' => 1,
    '#description' => 'Each Base DN will be queried and results merged.',
    '#default_value' => variable_get('wwu_email_autocomplete_basedn'),
  );

  return system_settings_form($form);
}
